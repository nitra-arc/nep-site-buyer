<?php

namespace Nitra\BuyerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReportInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('report_new_information', 'checkbox', array(
            'required'  => false,
            'label'     => 'report.info',
        ));
        $builder->add('submit', 'submit', array(
            'label'     => 'report.submit',
        ));
    }
    
    public function getName()
    {
        return 'report_info';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain'    => 'NitraBuyerBundle',
            'data_class'            => 'Nitra\BuyerBundle\Document\Buyer',
        ));
    }
}