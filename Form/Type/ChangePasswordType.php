<?php

namespace Nitra\BuyerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Form for change buyer password
 */
class ChangePasswordType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['current_password']) {
            $builder->add('current', 'password', array(
                'required' => true,
                'label'    => 'pass.current',
                'mapped'   => false,
            ));
        }
        $builder->add('new', 'repeated', array(
            'type'            => 'password',
            'invalid_message' => 'password.repeated',
            'required'        => true,
            'first_options'   => array(
                'label' => 'pass.new',
            ),
            'second_options'  => array(
                'label' => 'pass.newConf',
            ),
            'mapped'          => false,
        ));
        $builder->add('submit', 'submit', array(
            'label' => 'pass.submit',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'change_password';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'NitraBuyerBundle',
            'current_password'   => null,
        ));
    }
}