<?php

namespace Nitra\BuyerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DataEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array(
            'label'     => 'privat_office.info.name',
        ));
        $builder->add('phone', 'masked_input', array(
            'mask'      => '+38(999)999-99-99',
            'label'     => 'privat_office.info.phone',
        ));
        $builder->add('email', 'email', array(
            'label'     => 'privat_office.info.email',
        ));
        $builder->add('save', 'submit', array(
            'label'     => 'privat_office.info.save',
        ));
    }
    
    public function getName()
    {
        return 'edit_private_data';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain'    => 'NitraBuyerBundle',
            'data_class'            => 'Nitra\BuyerBundle\Document\Buyer',
        ));
    }
}