<?php

namespace Nitra\BuyerBundle\Lib;

use Symfony\Component\DependencyInjection\Container;
use Nitra\BuyerBundle\Document\Buyer;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Nitra\StoreBundle\Lib\Globals;

/**
 * класс, вызываемый сторовским прослушивателем запросов - обновляет время последней активности пользователя
 */
class BuyerActivity
{
    public static function udpateBuyerActivity(Container $container)
    {
        $request         = self::getRequest($container);
        $dm              = self::getDocumentManager($container);
        // получаем интервал обновления последней активности покупателя (если не указано - 1 минута, 0 - не обновлять);
        $updatedInterval = $container->hasParameter('user_activity_update_interval') ? $container->getParameter('user_activity_update_interval') : 1;
        if ($updatedInterval && $request) {
            $session     = $request->getSession();
            $buyer       = $session->has('buyer') ? $dm->getRepository('NitraBuyerBundle:Buyer')->find($session->get('buyer')['id']) : false;
            if ($buyer) {
                self::updateActivity($dm, $buyer);
                self::updateCart($dm, $buyer, $session);
            }
        }
    }

    /**
     * @param \Symfony\Component\DependencyInjection\Container $container
     * @return \Symfony\Component\HttpFoundation\Request
     */
    private static function getRequest(Container $container)
    {
        return $container->isScopeActive('request') ? $container->get('request') : false;
    }
    
    /**
     * @param \Symfony\Component\DependencyInjection\Container $container
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    private static function getDocumentManager(Container $container)
    {
        return $container->get('doctrine_mongodb.odm.document_manager');
    }
    
    private static function updateActivity(DocumentManager $dm, Buyer $buyer)
    {
        $buyer->setLastActivity(new \MongoDate(strtotime('now')));
        $dm->flush();
    }
    
    public static function updateCart(DocumentManager $dm, Buyer $buyer, Session $session)
    {
        if ($session->has('cart') || $session->has('sets')) {
            $ordersData = $buyer->getOrdersData();
            if (!key_exists('lastCartData', $ordersData)) {
                $ordersData['lastCartData'] = array();
            } else {
                unset($ordersData['lastCartData']['sended']);
            }
            if ($session->has('cart')) {
                $ordersData['lastCartData']['cart'] = $session->get('cart');
            }
            if ($session->has('sets')) {
                $ordersData['lastCartData']['sets'] = $session->get('sets');
            }
            $ordersData['lastCartData']['storeId'] = Globals::getStore()['id'];
            $buyer->setOrdersData($ordersData);
            $dm->flush();
        }
    }
}