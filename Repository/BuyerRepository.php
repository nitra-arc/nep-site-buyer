<?php

namespace Nitra\BuyerBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

/**
 * BuyerRepository
 */
class BuyerRepository extends DocumentRepository
{
    /**
     * @param string $hostName
     * @param \Nitra\BuyerBundle\Document\Buyer $Buyer
     */
    public function join($hostName, &$Buyer)
    {
        if ($Buyer->getTetradkaId()) {
            return true;
        } elseif ($Buyer->getPhone()) {
            $tBuyer = $this->getBuyerFromTetradka($hostName, null, $Buyer->getPhone());
            if (is_array($tBuyer) && key_exists('status', $tBuyer) && ($tBuyer['status'] == 'success')) {
                $Buyer->setTetradkaId($tBuyer['buyer']['id']);
                $this->getDocumentManager()->flush($Buyer);
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * @param string $hostName
     * @param string $productRepository
     * @param \Nitra\BuyerBundle\Document\Buyer $Buyer
     */
    public function getOrders($hostName, $productRepository, $Buyer)
    {
        if (!$Buyer->getTetradkaId()) {
            $this->join($hostName, $Buyer);
        }
        
        if ($Buyer->getTetradkaId()) {
            $tBuyer = $this->getBuyerFromTetradka($hostName, $Buyer->getTetradkaId(), null, true);
            return $this->formatOrders($tBuyer, $productRepository);
        } else {
            return array(
                'orders'            => array(),
                'orders_history'    => array(),
            );
        }
    }
    
    /**
     * @param string $hostName
     * @param \Nitra\BuyerBundle\Document\Buyer $Buyer
     */
    public function saveInTetradka($hostName, $Buyer)
    {
        $data = array(
            'id'    => $Buyer->getTetradkaId(),
            'name'  => $Buyer->getName(),
            'phone' => $Buyer->getPhone(),
            'email' => $Buyer->getEmail(),
        );
        
        return $this->tetradka("http://$hostName/order/update-buyer", $data);
    }
    
    /**
     * @param string    $hostName   Хост тетрадки
     * @param int       $id         Id покупателя в тетрадке
     * @param int       $phone      Телефон
     * @param bool      $orders     Получить заказы?
     * @return array or false
     */
    public function getBuyerFromTetradka($hostName, $id = null, $phone = null, $orders = false)
    {
        if (!$id && !$phone) {
            return false;
        }
        
        $data = array(
            'id'        => $id,
            'phone'     => $phone,
            'orders'    => $orders,
        );
        
        return $this->tetradka("http://$hostName/get-buyer-info-site", $data);
    }
    
    /**
     * @param array $tBuyer
     * @param string $productRepository
     * @return array
     */
    protected function formatOrders($tBuyer, $productRepository)
    {
        if (!is_array($tBuyer) || ($tBuyer['status'] != 'success')) {
            return array(
                'orders'            => array(),
                'orders_history'    => array(),
            );
        }
        
        $formated = array(
            'orders'            => $tBuyer['orders'],
            'orders_history'    => $tBuyer['orders_history'],
        );
        if (is_array($tBuyer)) {
            $this->processFormatOrders($formated['orders'],         $productRepository);
            $this->processFormatOrders($formated['orders_history'], $productRepository);
        }
        
        return $formated;
    }
    
    /**
     * @param array $orders
     * @param string $productRepository
     */
    protected function processFormatOrders(&$orders, $productRepository)
    {
        foreach (array_keys($orders) as $orderId) {
            foreach ($orders[$orderId]['orderEntries'] as $entrieId => $entrie) {
                $orders[$orderId]['orderEntries'][$entrieId]['product'] = $this->getDocumentManager()->find($productRepository, $entrie['productId']);
            }
        }
    }
    
    /**
     * @param string $host
     * @param array $params
     * @return array|null
     */
    protected function tetradka($host, $params = array())
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $host);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        $response = json_decode(curl_exec($ch), true);
        curl_close($ch);
        return $response;
    }
}