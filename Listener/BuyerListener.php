<?php

namespace Nitra\BuyerBundle\Listener;

use Doctrine\ODM\MongoDB\Event\OnFlushEventArgs;
use Symfony\Component\DependencyInjection\Container;

class BuyerListener
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container    = $container;
    }

    /**
     * @param \Doctrine\ODM\MongoDB\Event\OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $dm         = $args->getDocumentManager();
        $uow        = $dm->getUnitOfWork();
        $updates    = $uow->getScheduledDocumentUpdates();
        $inserts    = $uow->getScheduledDocumentInsertions();

        $documents  = array_merge($inserts, $updates);
        
        foreach ($documents as $entrie) {
            $buyerToTetradka = array(
                'buyers'    => array(),
            );
            if (($entrie instanceof \Nitra\BuyerBundle\Document\Buyer) && ($entrie->getPhone() || $entrie->getPassword())) {
                $buyerToTetradka['buyers'][] = $this->formatBuyerToTetradkaArray($entrie);
                if ($this->tetradka('order/buyers-synchronizer', http_build_query($buyerToTetradka))['inserted']) {
                    $tBuyer = $this->findBuyer($dm, $entrie);
                    if ($tBuyer) {
                        $tBuyer->setEmail($entrie->getEmail());
                        $tBuyer->setPassword($entrie->getPassword());
                        $tBuyer->setCityId($entrie->getCityId());
                        $tBuyer->setCityName($entrie->getCityName());
                        $tBuyer->setLastActivity($entrie->getLastActivity());
                        if ($entrie->getIdentity()) {
                            $tBuyer->setIdentity($entrie->getIdentity());
                        }

                        $cm = $dm->getClassMetadata(get_class($tBuyer));
                        $uow->computeChangeSet($cm, $tBuyer);
                        
                        $uow->remove($entrie);
                    }
                }
            }
        }
    }
    
    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     * @param \Nitra\BuyerBundle\Document\Buyer $buyer
     * @return \Nitra\BuyerBundle\Document\Buyer
     */
    protected function findBuyer($dm, $buyer)
    {
        return $dm->createQueryBuilder('NitraBuyerBundle:Buyer')
            ->field('name')->equals($buyer->getName())
//            ->field('email')->equals($buyer->getEmail())
            ->field('phone')->equals($buyer->getPhone())
            ->getQuery()->execute()->getSingleResult();
    }

    /**
     * @param string $path
     * @param array|null $data
     * @return array
     */
    protected function tetradka($path, $data = null)
    {
        $host = $this->container->hasParameter('tetradka') ? $this->container->getParameter('tetradka') : 'localhost';
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://$host/$path");
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        
        $cResponse = curl_exec($ch);
        curl_close($ch);

        return json_decode($cResponse, true) ? json_decode($cResponse, true) : $cResponse;
    }

    /**
     * @param \Nitra\BuyerBundle\Document\Buyer $Buyer
     * @return array
     */
    protected function formatBuyerToTetradkaArray($Buyer)
    {
        return array(
            'tetradkaId'        => $Buyer->getTetradkaId(),
            'name'              => $Buyer->getName(),
            'phone'             => $Buyer->getPhone(),
            'email'             => $Buyer->getEmail(),
            'city'              => array(
                'id'                => $Buyer->getCityId(),
                'name'              => $Buyer->getCityName()
            ),
            'address'           => $Buyer->getAddress(),
            'reportNewArticles' => $Buyer->getReportNewInformation(),
            'reportProducts'    => $Buyer->getReportProducts(),
            'status'            => $Buyer->getStatus(),
            'social'            => array(
                'id'                => $Buyer->getSocialId(),
                'net'               => $Buyer->getSocialNet(),
            ),
        );
    }
}