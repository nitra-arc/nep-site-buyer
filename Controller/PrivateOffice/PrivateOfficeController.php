<?php

namespace Nitra\BuyerBundle\Controller\PrivateOffice;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Form\FormError;
use Nitra\BuyerBundle\Form\Type as POTypes;

class PrivateOfficeController extends Controller
{
    protected $buyerRepository      = 'NitraBuyerBundle:Buyer';
    protected $productRepository    = 'NitraProductBundle:Product';
    
    /** @return \Doctrine\ODM\MongoDB\DocumentManager */
    protected function getDocumentManager() { return $this->container->get('doctrine.odm.mongodb.document_manager'); }

    protected function getTetradka()        { return $this->container->hasParameter('tetradka') ? $this->container->getParameter('tetradka') : 'localhost'; }
    
    protected function getDataEditType()    { return new POTypes\DataEditType();    }
    protected function getChangePassForm()  { return new POTypes\ChangePasswordType();    }
    protected function getReportInfoForm()  { return new POTypes\ReportInfoType();  }
    
    /**
     * @return \Nitra\BuyerBundle\Document\Buyer|null
     */
    protected function getBuyer()
    {
        if (!$this->getRequest()->getSession()->has('buyer')) {
            return false;
        }
        
        $id     = $this->getRequest()->getSession()->get('buyer')['id'];
        $repo   = $this->getDocumentManager()->getRepository($this->buyerRepository);
        $Buyer  = $repo->find($id);
        
        if (!$Buyer) {
            return false;
        }
        $repo->join($this->getTetradka(), $Buyer);
        
        return $Buyer;
    }

    /**
     * Генерация кнопок "Вход|Регистрация" или "Выход|Личный кабинет"
     * @Route("/buyer-enter-generator", name="buyer_enter_generator")
     * @Template("NitraBuyerBundle:PrivateOffice:enter.html.twig")
     */
    public function enterAction(Request $request)
    {
        return array(
            'buyer' => $request->getSession()->has('buyer'),
        );
    }
    
    /**
     * Личный кабинет
     * @Route("/privat_office_page", name="privat_office_page")
     * @Template("NitraBuyerBundle:PrivateOffice:privatOfficePage.html.twig")
     */
    public function indexAction()
    {
        if (!$this->getBuyer()) {
            return new RedirectResponse($this->generateUrl('nitra_store_home_index'));
        }
        
        return array();
    }
    
    /**
     * @Route("/my-data", name="my_data")
     * @Template("NitraBuyerBundle:PrivateOffice:buyerData.html.twig")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|array
     */
    public function buyerDataAction($page = true)
    {
        $Buyer = $this->getBuyer();
        if (!$Buyer) {
            return new RedirectResponse($this->generateUrl('nitra_store_home_index'));
        }
        
        return array(
            'page'              => $page,
            'buyer'             => $Buyer,
        );
    }
    
    /**
     * @Route("/reports-data", name="reports_data")
     * @Template("NitraBuyerBundle:PrivateOffice:reportsData.html.twig")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|array
     */
    public function reportsDataAction($page = true)
    {
        $Buyer = $this->getBuyer();
        if (!$Buyer) {
            return new RedirectResponse($this->generateUrl('nitra_store_home_index'));
        }
        
        $form = $this->getReportInformationForm($Buyer);
        
        return array(
            'page'              => $page,
            'buyer'             => $Buyer,
            'form'              => $form->createView(),
            'reportInform'      => $Buyer->getReportNewInformation(),
            'reportProducts'    => $this->formatReportProducts($Buyer),
        );
    }
    
    /**
     * @Route("/orders", name="orders")
     * @Template("NitraBuyerBundle:PrivateOffice:ordersData.html.twig")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function ordersDataAction($page = true)
    {
        $Buyer = $this->getBuyer();
        if (!$Buyer) {
            return new RedirectResponse($this->generateUrl('nitra_store_home_index'));
        }
        
        $orders = $this->getDocumentManager()->getRepository($this->buyerRepository)->getOrders($this->getTetradka(), $this->productRepository, $Buyer);
        
        return array_merge($orders, array(
            'page'              => $page,
            'buyer'             => $Buyer,
        ));
    }
    
    /**
     * сохранение поля "Уведомлять о новых статьях"
     * @Route("/privat-office-save-report-info", name="privat_office_save_report_info")
     */
    public function saveReportInfo(Request $request)
    {
        $Buyer = $this->getBuyer();
        $form  = $this->getReportInformationForm($Buyer);
        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $Buyer->setReportNewInformation($form->get('report_new_information')->getData());
            $this->getDocumentManager()->flush($Buyer);
            return new Response();
        }
        return new Response('', 404);
    }
    
    /**
     * удалить товар из уведомлений 
     * @Route("/privat-office-remove-report-product/{id}/{report}", name="privat_office_remove_report_product")
     */
    public function removeProductReportAction($id, $report)
    {
        $Buyer = $this->getBuyer();
        $products = $Buyer->getReportProducts();
        if (key_exists($id, $products)) {
            if (key_exists($report, $products[$id])) {
                $products[$id][$report] = false;
                $Buyer->setReportProducts($products);
                $this->getDocumentManager()->flush($Buyer);
                return new Response();
            }
        }
        return new Response('', 404);
    }
    
    /**
     * Смена пароль
     *
     * @Route("/private-office-change-pass", name="private_office_change_pass")
     * @Template("NitraBuyerBundle:PrivateOffice:changePass.html.twig")
     *
     * @param Request $request
     *
     * @return array Template context
     */
    public function changePass(Request $request)
    {
        $Buyer      = $this->getBuyer();
        if (!$Buyer) {
            throw $this->createNotFoundException('Buyer is not authorized');
        }
        $form       = $this->createForm($this->getChangePassForm(), null, array(
            'current_password' => $Buyer->getPassword(),
        ));
        $form->handleRequest($request);
        $msg        = null;
        if ($form->isSubmitted() && $form->isValid()) {
            if (($form->has('current')) && (hash('sha256', $form->get('current')->getData()) != $Buyer->getPassword())) {
                $form->get('current')->addError(new FormError($this->get('translator')->trans('errors.password.current', array(), 'NitraBuyerBundle')));
            } else {
                $Buyer->setPassword(hash('sha256', $form->get('new')->getData()));
                $this->getDocumentManager()->flush($Buyer);
                $msg = 'success';
            }
        }

        return array(
            'msg'   => $msg,
            'form'  => $form->createView(),
        );
    }
    
    /**
     * @Route("/edit-data", name="edit_buyer_data")
     * @Template("NitraBuyerBundle:PrivateOffice:editBuyerData.html.twig")
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function editBuyerDataAction(Request $request)
    {
        $Buyer  = $this->getBuyer();
        $form   = $this->createForm($this->getDataEditType(), $Buyer, array(
            'action'    => $this->generateUrl('edit_buyer_data'),
            'attr'      => array(
                'id'        => 'edit_data',
            ),
        ));
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDocumentManager()->flush($Buyer);
            return array(
                'msg'   => 'success',
            );
        }
        
        return array(
            'form'  => $form->createView(),
        );
    }
    
    /**
     * @param \Nitra\BuyerBundle\Document\Buyer $Buyer
     * @return array
     */
    protected function formatReportProducts($Buyer)
    {
        $reports = array(
            'priceDown' => array(),
            'quantity'  => array(),
        );
        $Products = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Product')
            ->field('_id')->in(array_keys($Buyer->getReportProducts()))
            ->getQuery()->execute()->toArray();
        foreach ($Buyer->getReportProducts() as $id => $report) {
            if ($report['priceDown']) {
                $reports['priceDown'][] = $Products[$id];
            }
            if (!$report['quantity'] && !$report['priceDown']) {
                $reports['quantity'][]  = $Products[$id];
            }
        }
        
        return $reports;
    }
    
    /**
     * @param \Nitra\BuyerBundle\Document\Buyer $Buyer
     * @return \Symfony\Component\Form\Form
     */
    protected function getReportInformationForm($Buyer)
    {
        return $this->createForm($this->getReportInfoForm(), $Buyer, array(
            'action'    => $this->generateUrl('privat_office_save_report_info'),
            'attr'      => array(
                'id'        => 'form_report_info',
            ),
        ));
    }
}