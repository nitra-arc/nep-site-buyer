<?php

namespace Nitra\BuyerBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Bundle\MongoDBBundle\Validator\Constraints\Unique;

/**
 * @MongoDB\Document(repositoryClass="Nitra\BuyerBundle\Repository\BuyerRepository")
 * @Unique(fields="email")
 * @Unique(fields="phone")
 */
class Buyer
{
    /**
     * @MongoDB\Id(strategy="AUTO")
     */
    private $id;

    /**
     * @MongoDB\Int
     */
    private $tetradkaId;

    /**
     * @MongoDB\String
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @MongoDB\Field(type="string")
     * @Assert\Choice(choices = {"active", "passive"})
     */
    private $status;

    /**
     * @MongoDB\String
     * @Assert\Email
     */
    private $email;

    /**
     * @MongoDB\String
     */
    private $phone;

    /**
     * @MongoDB\String
     */
    private $address;

    /**
     * @MongoDB\String
     */
    private $socialNet;

    /**
     * @MongoDB\String
     */
    private $socialId;

    /**
     * Уведомлять ли о новых статьях
     * @MongoDB\Boolean
     */
    private $report_new_information;

    /**
     * Массив товаров, о поступлении которых необходимо сообщить
     * @MongoDB\Hash
     */
    private $report_products = array();

    /**
     * @MongoDB\String
     */
    private $password;

    /**
     * @MongoDB\String
     */
    private $identity;

    /**
     * @MongoDB\String
     */
    private $cityName;

    /**
     * @MongoDB\Int
     */
    private $cityId;

    /**
     * @MongoDB\Hash
     */
    private $ordersData;

    /**
     * @MongoDB\Date
     */
    private $lastActivity;
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get tetradkaId
     * @return tetradkaId $tetradkaId
     */
    public function getTetradkaId()
    {
        return $this->tetradkaId;
    }

    /**
     * Set tetradkaId
     * @param string $tetradkaId
     * @return self
     */
    public function setTetradkaId($tetradkaId)
    {
        $this->tetradkaId = $tetradkaId;
        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     * @param string $address
     * @return self
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Get address
     * @return string $address
     */
    public function getAddress()
    {
        return $this->address;
    }

    public function getStatusView()
    {
        if ($this->status == 'active') {
            $this->statusView = "Активный";
        } elseif ($this->status == 'passive') {
            $this->statusView = "Пасивный";
        } else {
            $this->statusView = '';
        }
        return $this->statusView;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return string $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set socialNet
     *
     * @param string $socialNet
     * @return self
     */
    public function setSocialNet($socialNet)
    {
        $this->socialNet = $socialNet;
        return $this;
    }

    /**
     * Get socialNet
     *
     * @return string $socialNet
     */
    public function getSocialNet()
    {
        return $this->socialNet;
    }

    /**
     * Set socialId
     *
     * @param string $socialId
     * @return self
     */
    public function setSocialId($socialId)
    {
        $this->socialId = $socialId;
        return $this;
    }

    /**
     * Get socialId
     *
     * @return string $socialId
     */
    public function getSocialId()
    {
        return $this->socialId;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = preg_replace('/[^0-9]/', '', $phone);
        return $this;
    }

    /**
     * Get phone
     *
     * @return string $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set reportNewInformation
     *
     * @param boolean $reportNewInformation
     * @return self
     */
    public function setReportNewInformation($reportNewInformation)
    {
        $this->report_new_information = $reportNewInformation;
        return $this;
    }

    /**
     * Get reportNewInformation
     *
     * @return boolean $reportNewInformation
     */
    public function getReportNewInformation()
    {
        return $this->report_new_information;
    }

    /**
     * Set reportProducts
     *
     * @param hash $reportProducts
     * @return self
     */
    public function setReportProducts($reportProducts)
    {
        $this->report_products = $reportProducts;
        return $this;
    }

    /**
     * Get reportProducts
     *
     * @return hash $reportProducts
     */
    public function getReportProducts()
    {
        return $this->report_products;
    }


    /**
     * Set password
     *
     * @param string $password
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Get password
     *
     * @return string $password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set identity
     *
     * @param string $identity
     * @return self
     */
    public function setIdentity($identity)
    {
        $this->identity = $identity;
        return $this;
    }

    /**
     * Get identity
     *
     * @return string $identity
     */
    public function getIdentity()
    {
        return $this->identity;
    }

    /**
     * Set cityName
     *
     * @param string $cityName
     * @return self
     */
    public function setCityName($cityName)
    {
        $this->cityName = $cityName;
        return $this;
    }

    /**
     * Get cityName
     *
     * @return string $cityName
     */
    public function getCityName()
    {
        return $this->cityName;
    }

    /**
     * Set cityId
     *
     * @param int $cityId
     * @return self
     */
    public function setCityId($cityId)
    {
        $this->cityId = $cityId;
        return $this;
    }

    /**
     * Get cityId
     *
     * @return int $cityId
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * Set ordersData
     *
     * @param array $ordersData
     * @return self
     */
    public function setOrdersData($ordersData)
    {
        $this->ordersData = $ordersData;
        return $this;
    }

    /**
     * Get ordersData
     *
     * @return array $ordersData
     */
    public function getOrdersData()
    {
        return $this->ordersData ? $this->ordersData : array();
    }

    /**
     * Set lastActivity
     *
     * @param Date $lastActivity
     * @return self
     */
    public function setLastActivity($lastActivity)
    {
        $this->lastActivity = $lastActivity;
        return $this;
    }

    /**
     * Get lastActivity
     *
     * @return Date $lastActivity
     */
    public function getLastActivity()
    {
        return $this->lastActivity;
    }
}