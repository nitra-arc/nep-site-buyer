# BuyerBundle

## Описание
Бандл покупателей - содержит сам документ покупателя, контроллер личного кабинета и класс для обновления даты последнего посещения сайта покупателем (автоматически вызывается из прослушивателя в бандле магазина).

## Подключение

Для подключения данного модуля в проект необходимо:

* **composer.json**:

```json
{
    ...   
    "require": {
        ...
        "nitra/e-commerce-site-buyerbundle": "dev-master",
        ...
    }
    ...
}
```

* **app/AppKernel.php**:

```php
<?php

    //...
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\BuyerBundle\NitraBuyerBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

## Настройки
### **parameters.yml**
* Частота обновления даты последнего посещения покупателем
    * **user_activity_update_interval: 1** - по умолчанию 1 минута, если 0 - не обновлять
